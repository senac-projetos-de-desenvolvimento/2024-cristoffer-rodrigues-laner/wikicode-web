export default function setDataToLocalStorage (cliente) {
    if(typeof window !== 'undefined') {
        localStorage.setItem("user_logged", JSON.stringify({id: cliente.id, nome: cliente.name, permissao: cliente.permissao, token: cliente.token}))
    }
}