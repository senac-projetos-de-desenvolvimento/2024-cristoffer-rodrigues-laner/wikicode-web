export default function deleteDataFromLocalStorage () {
    if(typeof window !== 'undefined') {
        localStorage.removeItem('user_logged')
    }
}