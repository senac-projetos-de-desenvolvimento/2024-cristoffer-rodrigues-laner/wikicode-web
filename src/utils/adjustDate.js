import moment from 'moment';
import 'moment/locale/pt-br'

export const adjustDateToLOCFormat = (data) => {
    const dateAdjusted = moment(data).format('DD/MM/YYYY')
    return dateAdjusted
}