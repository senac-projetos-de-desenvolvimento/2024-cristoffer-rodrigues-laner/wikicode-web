export default function getDataFromLocalStorage () {
    let session
    if(typeof window !== 'undefined') {
        session = JSON.parse(localStorage.getItem("user_logged"))
    } else {
        session = null
    }
    console.log("getDataFromLocalStorage: " + session);
    
    return session
}