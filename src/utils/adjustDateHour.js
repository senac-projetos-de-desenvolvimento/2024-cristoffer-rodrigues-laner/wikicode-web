import moment from 'moment';
import 'moment/locale/pt-br'

export const adjustDateHourToLOCFormat = (data) => {
    const dateAdjusted = moment(data).locale('pt-BR').format('LLL')
    return dateAdjusted
}