'use client'
import getDataFromLocalStorage from "@/utils/getDataFromLocalStorage"
import { createContext, useEffect, useState } from "react"

export const ClienteContext = createContext()

function ClienteProvider({children}) {

  const [clienteId, setClienteId] = useState(null)
  const [clienteNome, setClienteNome] = useState("")
  const [token, setToken] = useState("")
  const [clientePermissao, setClientePermissao] = useState("")

  useEffect(() => {
    const session = getDataFromLocalStorage() || null
    if (session) {
      const cliente = getDataFromLocalStorage()
      setClienteId(cliente.id)
      setClienteNome(cliente.nome)
      setToken(cliente.token)
      setClientePermissao(cliente.permissao)
    }
  }, [])

  function mudaId(id) {
    setClienteId(id)
  }

  function mudaNome(nome) {
    setClienteNome(nome)
  }

  function mudaToken(token) {
    setToken(token)
  }

  function mudaPermissao(permissao) {
    setClientePermissao(permissao)
  }

  return (
    <ClienteContext.Provider value={{clienteId, clienteNome, clientePermissao , token, mudaId, mudaNome, mudaToken, mudaPermissao}}>
      {children}
    </ClienteContext.Provider>
  )
}

export default ClienteProvider