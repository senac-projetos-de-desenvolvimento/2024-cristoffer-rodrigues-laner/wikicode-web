'use client'
import { useParams, useRouter } from "next/navigation"
import { useContext, useEffect, useState } from "react"
import {adjustDateHourToLOCFormat} from "@/utils/adjustDateHour"
import './publication.css'
import Comments from "@/components/Comments"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import Loading from "@/components/Loading"
import { ClienteContext } from "@/contexts/cliente"
import dotenv from "dotenv"
import getDataFromLocalStorage from "@/utils/getDataFromLocalStorage"

dotenv.config()

export default function Publication() {
  const params = useParams()
  const [publication, setPublication] = useState({})
  const [isLoading, setIsLoading] = useState(true)
  const [removeLoading, setRemoveLoading] = useState(false)
  const [participants, setParticipants] = useState([])
  const [comment, setComment] = useState("")
  const [comments, setComments] = useState([])
  const router = useRouter()
  const { clienteId } = useContext(ClienteContext)
  const baseUrl = process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT ? process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT : process.env.NEXT_PUBLIC_BASE_URL_PRODUCTION;

  useEffect(() => {
    const session = getDataFromLocalStorage() || null
    // const session = localStorage.getItem("user_logged") ? JSON.parse(localStorage.getItem("user_logged")) : null;
    async function getPublication() {
      if(session === null || session === undefined || !session) {
        router.push('/')
      }
      const response = await fetch(`${baseUrl}/publications/search_id/` + params.id, {
        method: 'GET',
        headers: {
          "Content-type": "application/json",
          "Authorization": "Bearer" + " " + session.token
        }
      })
      const dados = await response.json()
      let participantesArr = []
      dados.Participants.map((participant) => {
        participantesArr.push(participant.name)
      })

      const comments = await fetch(`${baseUrl}/reviews/search_id/` + params.id, {
        method: 'GET',
        headers: {
          "Content-type": "application/json",
          "Authorization": "Bearer" + " " + session.token
        }
      })
      const dados2 = await comments.json()

      setParticipants(participantesArr)
      setPublication(dados)
      setComments(dados2)
      setIsLoading(false)
      setRemoveLoading(true)
      document.title = dados.title
    }
    getPublication()
  }, [comment])


  if (isLoading) {
    return (
      <div className="container">
      </div>
    )
  }

  const dateAdjusted = adjustDateHourToLOCFormat(publication.createdAt)

  const onChangeHandler = (e) => {
        setComment(e.target.value);
  }

  const createReview = async () => {
        const session = getDataFromLocalStorage() || null
        // const session = localStorage.getItem("user_logged") ? JSON.parse(localStorage.getItem("user_logged")) : null;
        const user_id = session.id
        const publication_id = params.id
        const stars = 5

        const review = await fetch(`${baseUrl}/reviews/create`, {
                method: "POST",
                headers: {
                    "Content-type": "application/json",
                    "Authorization": "Bearer" + " " + session.token
                },
                body: JSON.stringify({ comment, user_id, publication_id, stars })
            }
        )
        if (review.status == 201) {
            toast.success("Comentário adicionado com sucesso!")
        } else {
            toast.error(publication.error)
        }
  }

  const deleteReview = async (id) => {
    const session = getDataFromLocalStorage() || null
    // const session = localStorage.getItem("user_logged") ? JSON.parse(localStorage.getItem("user_logged")) : null;
    const response = await fetch(`${baseUrl}/reviews/delete/` + id, {
      method: "DELETE",
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer" + " " + session.token
      }
    })
    if(response.status === 200) {
      const dados = await response.json()
      setComments(dados)
      setRemoveLoading(true)
      toast.success("Comentário excluído com sucesso!")
    } else {
      toast.error("Sem autorização.")
      return
    }
    const novosDados = comments.filter(publi => publi.id != id)
    setComments(novosDados)
  }

  const listComments = comments.map(comment => (
    <Comments key={comment.id}
      comment={comment}
      exclui={() => deleteReview(comment.id)}
    />
  ))

  return (
    <div className='content'>
      <div className="container px-4 py-4">
        <h4 className="tile text-wrap">{publication.title}</h4>
        <h5 className="subtitle">{publication.subtitle}</h5>
        <div className="d-flex flex-wrap gap-1 gap-lg-3 flex-lg-row">
            <div className="date d-flex align-center justify-center gap-2 m-0">
                <span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-calendar3" viewBox="0 0 16 16">
                        <path d="M14 0H2a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2M1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857z"/>
                        <path d="M6.5 7a1 1 0 1 0 0-2 1 1 0 0 0 0 2m3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2m3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2m-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2m3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2m3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2m3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2m-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2m3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2m3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2"/>
                    </svg>
                </span>
                <p>Publicado em {dateAdjusted}</p>
            </div>
            <div className="author d-flex align-center justify-center gap-2 m-0">
                <span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-person-fill" viewBox="0 0 16 16">
                        <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6"/>
                    </svg>
                </span>
                <p>por {publication.user.name}</p>
            </div>
            {
              participants.length > 0 ?
                  <div className="participants d-flex gap-2">
                      <span>
                          <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-people-fill" viewBox="0 0 16 16">
                              <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6m-5.784 6A2.24 2.24 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.3 6.3 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1zM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5"/>
                          </svg>
                      </span>
                      <p>participantes: {participants.join(", ")}</p>
                  </div>
              : 
                  ""
              }
        </div>
        <div className="description mt-3">
          <div dangerouslySetInnerHTML={{ __html: publication.description }} />
        </div>
      </div>
      <div className="container px-4 py-4">
        <hr />
        <h3>Comentários</h3>
      </div>
      {!removeLoading && <Loading />}
      {listComments.length === 0 && removeLoading ? <p className="container px-4">Não há comentários para exibir.</p>  : listComments}
      <div className="container px-4 py-4">
            <div className="d-flex flex-column gap-3 w-100 w-lg-50">
                <h3>Deixe seu comentário!</h3>
                <textarea
                    id="textarea"
                    value={comment}
                    onChange={onChangeHandler}
                    className="commentbox"
                >
                </textarea>
                <button className="btn btn-success w-25 w-lg-25" onClick={() => {
                    setComment("")
                    createReview()
                }}>
                    Comente
                </button>
            </div>
      </div>
        <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="colored"
        />
    </div>
  )
}