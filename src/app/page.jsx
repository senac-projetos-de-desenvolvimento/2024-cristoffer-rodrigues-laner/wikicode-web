'use client'
import './login.css'
import { useForm } from 'react-hook-form'
import { useContext, useEffect, useState } from 'react'
import { ClienteContext } from '../contexts/cliente.jsx'
import { useRouter } from 'next/navigation'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import Image from 'next/image'
import Button from '@/components/Button'
import dotenv from "dotenv"
import setDataToLocalStorage from '@/utils/setDataToLocalStorage'
import getDataFromLocalStorage from '@/utils/getDataFromLocalStorage'

dotenv.config()

export default function Login() {
  const { register, handleSubmit } = useForm()
  const { mudaId, mudaNome, mudaToken, mudaPermissao } = useContext(ClienteContext)
  const [isShow, setIsShow] = useState(false)
  const [loadingButton, setloadingButton] = useState(false)
  const router = useRouter()
  const baseUrl = process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT ? process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT : process.env.NEXT_PUBLIC_BASE_URL_PRODUCTION;
  
  const handlePassword = () => setIsShow(!isShow)
  const handleClick = () => {
    setloadingButton(true)
    setTimeout(() => {
      setloadingButton(false)
    }, 5000);
  }

  useEffect(() => {
    const session = getDataFromLocalStorage() || null
    // const session = JSON.parse(localStorage.getItem("user_logged"))

    if(!!session) {
      router.push('/home')
    }
  }, [])

  async function verificaLogin(data) {
    
    const response = await fetch(`${baseUrl}/login`,
      {
        method: "POST",
        headers: { "Content-type": "application/json" },
        body: JSON.stringify({username: data.username, password: data.password})
      },
    )
    
    if (response.status == 401) {
      toast.error("Usuário ou senha inválidos.")
      return
    } else {
      const cliente = await response.json()

      mudaId(cliente.id)
      mudaNome(cliente.name)
      mudaToken(cliente.token)
      mudaPermissao(cliente.permissao)

      setDataToLocalStorage(cliente)
      // localStorage.setItem("user_logged", JSON.stringify({id: cliente.id, nome: cliente.name, permissao: cliente.permissao, token: cliente.token}))
      router.push("/home")
    }
  }

  return (
    <div className='d-flex w-100 h-100 align-items-center justify-content-center' id='background-login'>
      <main className="form-signin w-100 mx-4 border rounded">
        <form onSubmit={handleSubmit(verificaLogin)}>
          <Image src="/assets/Logo wikicode.png" priority alt="Logo" width="240" height="36" className="d-block m-auto" />
          <h1 className="h3 mb-4 fw-normal mt-4 text-center">Bem-vindo ao Wikicode!</h1>
          <p className="h3 pb-3 fs-6 fw-normal text-center">Use usuário e senha para acessar a sua base de conhecimento!</p>
          <div className="form-floating">
            <input type="text" className="form-control" id="floatingInput" placeholder="digite seu usuário..." 
            required {...register("username")} />
            <label htmlFor="floatingInput">Usuário</label>
          </div>
          <div className="form-floating mt-3">
            <input type={isShow ? "text" : "password"} className="form-control" id="floatingPassword" placeholder="********" 
              required {...register("password")} />
            <label htmlFor="floatingPassword">Senha de Acesso</label>
          </div>
          <div class="form-check mt-3">
            <input class="form-check-input" type='checkbox' value="" id="flexCheckDefault" onClick={handlePassword} />
            <label class="form-check-label" for="flexCheckDefault">
              Mostrar senha
            </label>
          </div>

          <Button 
            title={"Entrar"}
            loadingButton={loadingButton}
            onClick={handleClick}
            color={"primary"}
          />
        </form>
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="colored"
        />
      </main>
    </div>
  )
}