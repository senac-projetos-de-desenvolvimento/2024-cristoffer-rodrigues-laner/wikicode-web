'use client'

import PublicationList from "@/components/PublicationList"
import "../globals.css"
import { useContext, useEffect, useState } from "react"
import NoPublications from "@/components/NoPublications"
import Loading from "@/components/Loading"
import { useRouter } from 'next/navigation'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import Pesquisa from "@/components/Pesquisa"
import Swal from "sweetalert2"
import { ClienteContext } from "@/contexts/cliente"
import dotenv from "dotenv"
import getDataFromLocalStorage from "@/utils/getDataFromLocalStorage"

dotenv.config()

export default function Home() {
  const [publication, setPublication] = useState([])
  const [removeLoading, setRemoveLoading] = useState(false)
  const router = useRouter()
  const { clientePermissao } = useContext(ClienteContext)
  const baseUrl = process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT ? process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT : process.env.NEXT_PUBLIC_BASE_URL_PRODUCTION;
  
  if(clientePermissao == null || clientePermissao == '') {
    router.push('/')
  }

  useEffect(() => {
    const session = getDataFromLocalStorage() || null
    // const session = localStorage.getItem("user_logged") ? JSON.parse(localStorage.getItem("user_logged")) : null;
    async function getPublication() {
      try {
        const response = await fetch(`${baseUrl}/publications`, {
          method: 'GET',
          headers: {
            "Content-type": "application/json",
            "Authorization": "Bearer" + " " + session.token
          }
        })
        
        if(response.status === 200) {
          const dados = await response.json()
          setPublication(dados)
          setRemoveLoading(true)
        } else {
          toast.error("Sem autenticação.")
        }
      } catch (error) {
        router.push('/')
      }
    }
    getPublication()
  }, [])

  const listPublications = publication.map(publi => (
    <PublicationList key={publi.id}
      publication={publi}
    />
  ))

  async function filtraDados(data) {
    const session = getDataFromLocalStorage() || null
    // const session = localStorage.getItem("user_logged") ? JSON.parse(localStorage.getItem("user_logged")) : null;
    if (data.pesq.length < 2) {
      Swal.fire("Digite, no mínimo, 2 caracteres")
      return
    }

    const pesquisa = data.pesq.toUpperCase()
    
    const response = await fetch(`${baseUrl}/publications`, {
      method: 'GET',
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer" + " " + session.token
      }
    })
    const dados = await response.json()

    const novosDados = dados.filter(publi =>
      publi.title.toUpperCase().includes(pesquisa) || publi.subtitle.toUpperCase().includes(pesquisa) || publi.description.toUpperCase().includes(pesquisa) || publi.user.name.toUpperCase().includes(pesquisa)
    )

    if (novosDados.length == 0) {
      Swal.fire("Não há publicações com a palavra chave informada...")
      return
    }

    setPublication(novosDados)
  }

  async function mostraTodos() {
    const session = getDataFromLocalStorage() || null
    // const session = localStorage.getItem("user_logged") ? JSON.parse(localStorage.getItem("user_logged")) : null;
    const response = await fetch(`${baseUrl}/publications`, {
      method: 'GET',
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer" + " " + session.token
      }
    })
    const dados = await response.json()
    setPublication(dados)
  }

  return (
    <>
      <div className='content'>
        <div className="container">
          <section>
            <div className="py-4">
              <Pesquisa filtra={filtraDados} mostra={mostraTodos} />
            </div>
            <ul className="py-0 px-0 my-0 mx-0">
              {!removeLoading && <Loading />}
              {listPublications.length === 0 && removeLoading ? <NoPublications /> : listPublications}
            </ul>
          </section>
          <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
              theme="colored"
            />
        </div>
      </div>
    </>
  )
}