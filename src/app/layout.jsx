import Header from '@/components/Header'
import Navbar from '@/components/Navbar'
import 'bootstrap/dist/css/bootstrap.css'
import './globals.css'
import ClienteProvider from '@/contexts/cliente'

export const metadata = {
  title: 'Wikicode - Admin',
  description: 'Knowledge Base',
}

export default function RootLayout({ children }) {
  return (
    <html lang="pt-br">  
    <head>
      <link rel="shortcut icon" href="/assets/favicon.ico"/>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css"></link>
    </head>    
      <body>
        <ClienteProvider>
          <Header />
          <Navbar />
          {children}

        </ClienteProvider>
      </body>
    </html>
  )
}