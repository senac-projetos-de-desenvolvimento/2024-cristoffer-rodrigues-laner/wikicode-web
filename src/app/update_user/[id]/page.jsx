'use client'
import { ClienteContext } from "@/contexts/cliente";
import { useParams, useRouter } from "next/navigation";
import { useContext, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { ToastContainer, toast } from 'react-toastify';
import dotenv from "dotenv"

dotenv.config()

import 'react-toastify/dist/ReactToastify.css'
import getDataFromLocalStorage from "@/utils/getDataFromLocalStorage";

export default function UpdateUser() {
    const params = useParams()
    const { register, handleSubmit, reset } = useForm()
    const { clientePermissao } = useContext(ClienteContext)
    const router = useRouter()
    const session = getDataFromLocalStorage() || null
    // const session = JSON.parse(localStorage.getItem("user_logged"))
    const [isShow, setIsShow] = useState(false)
    const handlePassword = () => setIsShow(!isShow)
    const baseUrl = process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT ? process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT : process.env.NEXT_PUBLIC_BASE_URL_PRODUCTION;

    useEffect(() => {
        if(clientePermissao === null || clientePermissao === undefined || !clientePermissao) {
          router.push('/')
        }
        reset()
        try {
            async function getUser() {
              const response = await fetch(`${baseUrl}/users/search_id/` + params.id, {
                method: 'GET',
                headers: {
                  "Content-type": "application/json",
                  "Authorization": "Bearer" + " " + session.token
                }
              })
              const data = await response.json()

              if(response.status === 200) {
                  reset({
                    name: data.name,
                    username: data.username,
                    permission: data.permission
                  })
              } else {
                  toast.error("Sem autenticação.")
                  return
              }
            }
            getUser()
            
        } catch (error) {
            toast.error("Erro.")
            return
        }
    }, [])

    async function update(data) {
        const response = await fetch(`${baseUrl}/users/update/` + params.id,
            {
            method: "PUT",
            headers: {
              "Content-type": "application/json",
              "Authorization": "Bearer" + " " + session.token
            },
            body: JSON.stringify({ ...data })
            },
        )
        if (response.status == 201) {
            toast.success("Usuário alterado com sucesso!")
        } else {
            toast.error("Erro... Não foi possível concluir a alteração.")
        }
    }

    return (
        <div className='content'>
          {clientePermissao === "Master" &&
          <div className="container">
            <h2 className="mt-2">Alterar Usuário</h2>
            <form onSubmit={handleSubmit(update)}>
              <div className="row">
                <div className="col-sm-6">
                  <label htmlFor="name" className="form-label">Nome:</label>
                  <input type="text" className="form-control" id="name" placeholder="Escreva um nome..." {...register("name")} required />
                </div>
                <div className="col-sm-6">
                  <label htmlFor="username" className="form-label">Usuário:</label>
                  <input type="text" step="0.10" className="form-control" id="username" placeholder="Digite um login..." {...register("username")} required />
                </div>
                <div className="col-sm-6 mt-3">
                  <label htmlFor="password" className="form-label">Senha:</label>
                  <input type={isShow ? "text" : "password"} step="0.10" className="form-control" id="password" placeholder="Nova Senha..." {...register("password")} required autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" />
                </div>
              </div>
              <div class="form-check mt-3">
                <input class="form-check-input" type='checkbox' value="" id="flexCheckDefault" onClick={handlePassword} />
                <label class="form-check-label" for="flexCheckDefault">
                  Mostrar senha
                </label>
              </div>
      
              <div className="row my-3">
                <div className="col-sm-4">
                  <label htmlFor="permission" className="form-label">Permissão:</label>
                  <select id="permission" className="form-select" {...register("permission")} required>
                    <option value="" selected disabled>Selecione</option>
                    <option value="Leitor">Leitor</option>
                    <option value="Editor">Editor</option>
                    <option value="Master">Master</option>
                  </select>
                </div>
              </div>
      
              <input type="submit" value="Alterar" className="btn btn-success me-3" />
              <input type="button" value="Voltar" className="btn btn-warning"
                            onClick={() => router.push('/users')} />
            </form>
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
              theme="colored"
            />
          </div>
          }
        </div>
      )
}