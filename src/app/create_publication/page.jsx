'use client'
import { ClienteContext } from "@/contexts/cliente";
import { useRouter } from "next/navigation";
import { use, useContext, useEffect, useRef, useState } from "react";
import { useForm } from "react-hook-form";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import Select from 'react-select';
import makeAnimated from 'react-select/animated';import dotenv from "dotenv"

dotenv.config()

const animatedComponents = makeAnimated();

import { Editor } from '@tinymce/tinymce-react';
import getDataFromLocalStorage from "@/utils/getDataFromLocalStorage";


export default function CreatePublication() {
    const editorRef = useRef(null);
    const { register, handleSubmit, reset } = useForm({});
    const { clientePermissao } = useContext(ClienteContext)
    const [selectedParticipants, setSelectedParticipants] = useState([])
    const [participantsOptions, setParticipantesOptions] = useState([])
    const [categoriesOptions, setCategoriesOptions] = useState([])
    const [selectedCategory, setSelectedCategory] = useState([])
    const [visibility, setVisibility] = useState(null)
    const router = useRouter()
    const session = getDataFromLocalStorage() || null
    // const session = JSON.parse(localStorage.getItem("user_logged"))
    const baseUrl = process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT ? process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT : process.env.NEXT_PUBLIC_BASE_URL_PRODUCTION;


    useEffect(() => {
        if (clientePermissao === null || clientePermissao === undefined || !clientePermissao) {
            router.push('/')
        }

        async function refreshOptions() {
            const users = await fetch(`${baseUrl}/users/names`, {
                method: "GET",
                headers: {
                    "Content-type": "application/json",
                    "Authorization": "Bearer" + " " + session.token
                }
            })
            const usersResponse = await users.json()
            const usersOptions = usersResponse.map(user => ({
                'value': user.id,
                'label': user.name
            }))

            const categories = await fetch(`${baseUrl}/categories`, {
                method: "GET",
                headers: {
                    "Content-type": "application/json",
                    "Authorization": "Bearer" + " " + session.token
                }
            })
            const categoriesResponse = await categories.json()
            const categoriesOptions = categoriesResponse.map(category => ({
                'value': category.id,
                'label': category.name
            }))

            setParticipantesOptions(usersOptions)
            setCategoriesOptions(categoriesOptions)
        }

        refreshOptions()
    }, [])

    async function create(data) {
        const description = editorRef.current.getContent()
        const user_id = session.id

        let participants = []
        let category_id = selectedCategory.value
        let isPublic = visibility

        selectedParticipants.map(participant => {
            participants.push(participant.value)
        })

        const publication = await fetch(`${baseUrl}/publications/create`,
            {
                method: "POST",
                headers: {
                    "Content-type": "application/json",
                    "Authorization": "Bearer" + " " + session.token
                },
                body: JSON.stringify({ ...data, description, user_id, participants, isPublic, category_id })
            },
        )
        if (publication.status == 201) {
            toast.success("Publicação cadastrada com sucesso!")
            reset()
        } else {
            toast.error(publication.error)
        }
    }

    return (
        <div className='content'>
            {!(clientePermissao === "Leitor") &&
                <div className="container">
                    <h2 className="mt-2">Cadastrar Publicação</h2>
                    <form onSubmit={handleSubmit(create)}>
                        <div className="row">
                            <div className="col-sm-6">
                                <label htmlFor="title" className="form-label">Título da Publicação: <span className="text-danger fw-bold">*</span></label>
                                <input type="text" className="form-control" id="title" {...register("title")} required />
                            </div>
                            <div className="col-sm-6 mt-4 mt-sm-0">
                                <label htmlFor="subtitle" className="form-label">Subtítulo da Publicação: <span className="text-danger fw-bold">*</span></label>
                                <input type="text" step="0.10" className="form-control" id="subtitle" {...register("subtitle")} required />
                            </div>
                        </div>

                        <div className="row mt-4">
                            <div className="col-sm-6">
                                <label htmlFor="category" className="form-label">Categoria: <span className="text-danger fw-bold">*</span></label>
                                <Select
                                    closeMenuOnSelect={true}
                                    components={animatedComponents}
                                    isSearchable
                                    options={categoriesOptions}
                                    onChange={(item) => setSelectedCategory(item)}
                                    className="z-3"
                                    placeholder="Selecione..."
                                    required
                                />
                            </div>
                            <div className="col-sm-6 mt-4 mt-sm-0">
                                <label htmlFor="" className="form-label">Participantes:</label>
                                <Select
                                    closeMenuOnSelect={false}
                                    components={animatedComponents}
                                    isMulti
                                    options={participantsOptions}
                                    onChange={(item) => setSelectedParticipants(item)}
                                    className="z-3"
                                    placeholder="Selecione..."
                                />
                            </div>
                        </div>

                        <div className="mb-3 mt-4">
                            <label htmlFor="" className="form-label">Descrição: <span className="text-danger fw-bold">*</span></label>
                            <Editor
                                apiKey='ripemour1w42a5x27zbm6zhhi15g0o8o4smfk2xqe0422oqo'
                                onInit={(_evt, editor) => editorRef.current = editor}
                                initialValue=""
                                init={{
                                    height: 300,
                                    menubar: false,
                                    plugins: [
                                        'advlist', 'autolink', 'lists', 'link', 'image', 'charmap', 'preview',
                                        'anchor', 'searchreplace', 'visualblocks', 'code', 'fullscreen',
                                        'insertdatetime', 'media', 'table', 'code', 'help', 'wordcount'
                                    ],
                                    toolbar: 'undo redo | blocks | ' +
                                        'bold italic forecolor | alignleft aligncenter ' +
                                        'alignright alignjustify | bullist numlist outdent indent | ' +
                                        'removeformat | help',
                                    content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
                                }}
                            />
                        </div>

                        <div className="row mt-4">
                            <div className="col-sm-12">
                                <p>Visibilidade: <span className="text-danger fw-bold">*</span></p>
                                <div className="form-check">
                                    <input className="form-check-input" type="radio" name="exampleRadios"
                                        id="exampleRadios1" value="0" onChange={e => setVisibility(e.target.value)} required />
                                    <label className="form-check-label" for="exampleRadios1">
                                        Interno
                                    </label>
                                </div>
                                <div className="form-check">
                                    <input className="form-check-input" type="radio" name="exampleRadios"
                                        id="exampleRadios2" value="1" onChange={e => setVisibility(e.target.value)} required />
                                    <label className="form-check-label" for="exampleRadios2">
                                        Público
                                    </label>
                                </div>
                            </div>
                        </div>

                        <input type="submit" value="Cadastrar" className="btn btn-success me-3 my-3" />
                        <input type="button" value="Limpar" className="btn btn-danger me-3 my-3"
                            onClick={() => {
                                reset({
                                    title: "",
                                    subtitle: "",
                                    category: "",
                                    user_id: ""
                                })
                                editorRef.current.setContent("")
                                setSelectedParticipants([])
                                setSelectedCategory([])
                            }} />
                        <input type="button" value="Voltar" className="btn btn-warning"
                            onClick={() => router.push('/publications')} />
                    </form>
                    <ToastContainer
                        position="top-right"
                        autoClose={5000}
                        hideProgressBar={false}
                        newestOnTop={false}
                        closeOnClick
                        rtl={false}
                        pauseOnFocusLoss
                        draggable
                        pauseOnHover
                        theme="colored"
                    />
                </div>
            }
        </div>

    )
}