'use client'
import { useContext, useEffect, useState } from "react"
import { useRouter } from "next/navigation"
import NoPublications from "@/components/NoPublications"
import Loading from "@/components/Loading"
import Pesquisa from "@/components/Pesquisa"
import Swal from "sweetalert2"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import { ClienteContext } from "@/contexts/cliente"
import CategoriesListTable from "@/components/CategoriesListTable"
import dotenv from "dotenv"
import getDataFromLocalStorage from "@/utils/getDataFromLocalStorage"

dotenv.config()

export default function Publications() {
  const [categories, setCategories] = useState([])
  const [removeLoading, setRemoveLoading] = useState(false)
  const { clientePermissao } = useContext(ClienteContext)
  const router = useRouter()
  const session = getDataFromLocalStorage() || null
  // const session = JSON.parse(localStorage.getItem("user_logged"))
  const baseUrl = process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT ? process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT : process.env.NEXT_PUBLIC_BASE_URL_PRODUCTION;


  useEffect(() => {
    // if(clientePermissao === null || clientePermissao === undefined || !clientePermissao) {
    //   router.push('/')
    // }
    async function getCategories() {
      try {
        const response = await fetch(`${baseUrl}/categories`, {
          method: 'GET',
          headers: {
            "Content-type": "application/json",
            "Authorization": "Bearer" + " " + session.token
          }
        })
        
        if(response.status === 200) {
          const dados = await response.json()
          setCategories(dados)
          setRemoveLoading(true)
        } else {
          toast.error("Sem autenticação.")
          return
        }
      } catch (error) {
        toast.error("Erro.")
        return
      }
    }
    getCategories()
  }, [])

  async function deleteCategory(id) {
    const response = await fetch(`${baseUrl}/categories/delete/` + id, {
      method: "DELETE",
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer" + " " + session.token
      }
    })
    if(response.status === 201) {
      const dados = await response.json()
      setCategories(dados)
      setRemoveLoading(true)
    } else {
      toast.error("Sem autorização.")
      return
    }
    const novosDados = categories.filter(category => category.id != id)
    setCategories(novosDados)
  }

  async function filtraDados(data) {
    if (data.pesq.length < 2) {
      Swal.fire("Digite, no mínimo, 2 caracteres")
      return
    }

    const pesquisa = data.pesq.toUpperCase()
    
    const response = await fetch(`${baseUrl}/categories`, {
      method: 'GET',
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer" + " " + session.token
      }
    })
    const dados = await response.json()

    const novosDados = dados.filter(category =>
        category.name.toUpperCase().includes(pesquisa)
    )

    if (novosDados.length == 0) {
      Swal.fire("Não há categorias com a palavra chave informada...")
      return
    }

    setCategories(novosDados)
  }

  async function mostraTodos() {
    const response = await fetch(`${baseUrl}/categories`, {
      method: 'GET',
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer" + " " + session.token
      }
    })
    const dados = await response.json()
    setCategories(dados)
  }

  const listCategories = categories.map(category => (
    <CategoriesListTable key={category.id}
      category={category}
      exclui={() => deleteCategory(category.id)}
      altera={() => router.push('update_category/' + category.id)}
      consulta={() => router.push('publication/' + category.id)}
    />
  ))

  return (
    <div className='content'>
      {!(clientePermissao === "Leitor") &&
          <div className="container">
            <div className="row mt-3">
              <div className="col-12 text-center col-sm-12 col-md-2">
                <h2 className="mt-2">Categorias</h2>
              </div>
              <div className="col-12 col-sm-8 col-md-8 d-flex align-items-center justify-content-center">
                <Pesquisa filtra={filtraDados} mostra={mostraTodos} />
              </div>
              <div className="col-12 my-3 col-sm-4 col-md-2 d-flex align-items-center justify-content-center">
                <button className="btn btn-success" 
                  onClick={() => router.push("/create_category")}>
                    Cadastrar
                </button>
              </div>
            </div>
            <table className="table table-striped">
              <thead>
                <tr className='border-bottom'>
                  <th className='border-0'>Ações</th>
                  <th className='border-0'>Categoria</th>
                </tr>
              </thead>
              <tbody>
                {listCategories}
              </tbody>
            </table>
            {!removeLoading && <Loading />}
            {listCategories.length === 0 && removeLoading ? <NoPublications /> : null}
        </div>
      }
      <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="colored"
          />
    </div>
  )
}