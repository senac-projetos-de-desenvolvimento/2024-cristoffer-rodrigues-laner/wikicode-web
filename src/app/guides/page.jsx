'use client'

import PublicationList from "@/components/PublicationList"
import "../globals.css"
import { useEffect, useState } from "react"
import NoPublications from "@/components/NoPublications"
import Loading from "@/components/Loading"
import { useRouter } from "next/navigation"
import Swal from "sweetalert2"
import Pesquisa from "@/components/Pesquisa"
import dotenv from "dotenv"
import getDataFromLocalStorage from "@/utils/getDataFromLocalStorage"

dotenv.config()

export default function Guides() {
  const [publication, setPublication] = useState([])
  const [removeLoading, setRemoveLoading] = useState(false)
  const router = useRouter()
  const session = getDataFromLocalStorage() || null
  // const session = JSON.parse(localStorage.getItem("user_logged"))
  const baseUrl = process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT ? process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT : process.env.NEXT_PUBLIC_BASE_URL_PRODUCTION;

  useEffect(() => {
    async function getPublication() {
      const response = await fetch(`${baseUrl}/publications/search_category/guia`, {
        method: 'GET',
        headers: {
          "Content-type": "application/json",
          "Authorization": "Bearer" + " " + session.token
        }
      })
      const dados = await response.json()
      setPublication(dados)
      setRemoveLoading(true)
    }
    getPublication()

    if(session === null || session === undefined || !session) {
      router.push('/')
    }
  }, [])

  const listPublications = publication.map(publi => (
    <PublicationList key={publi.id}
      publication={publi}
    />
  ))

  async function filtraDados(data) {
    if (data.pesq.length < 2) {
      Swal.fire("Digite, no mínimo, 2 caracteres")
      return
    }

    const pesquisa = data.pesq.toUpperCase()
    
    const response = await fetch(`${baseUrl}/publications/search_category/guia`, {
      method: 'GET',
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer" + " " + session.token
      }
    })
    const dados = await response.json()

    const novosDados = dados.filter(publi =>
      publi.title.toUpperCase().includes(pesquisa) || publi.subtitle.toUpperCase().includes(pesquisa) || publi.description.toUpperCase().includes(pesquisa) || publi.user.name.toUpperCase().includes(pesquisa)
    )

    if (novosDados.length == 0) {
      Swal.fire("Não há publicações com a palavra chave informada...")
      return
    }

    setPublication(novosDados)
  }

  async function mostraTodos() {
    const response = await fetch(`${process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT ? process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT : process.env.NEXT_PUBLIC_BASE_URL_PRODUCTION}/publications/search_category/guia`, {
      method: 'GET',
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer" + " " + session.token
      }
    })
    const dados = await response.json()
    setPublication(dados)
  }

  return (
    <section className='content'>
      <div className="container">
        <div className="py-4">
          <Pesquisa filtra={filtraDados} mostra={mostraTodos} />
        </div>
        <ul className="py-0 px-0 my-0 mx-0">
          {!removeLoading && <Loading />}
          {listPublications.length === 0 && removeLoading ? <NoPublications /> : listPublications}
        </ul>

      </div>
    </section>
  )
}