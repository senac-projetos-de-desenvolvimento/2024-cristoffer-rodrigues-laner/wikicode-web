'use client'
import { useContext, useEffect, useState } from "react"
import { useRouter } from "next/navigation"
import NoPublications from "@/components/NoPublications"
import Loading from "@/components/Loading"
import Pesquisa from "@/components/Pesquisa"
import Swal from "sweetalert2"
import UsersListTable from "@/components/UsersListTable"
import { ClienteContext } from "@/contexts/cliente"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import './style.css'
import dotenv from "dotenv"
import getDataFromLocalStorage from "@/utils/getDataFromLocalStorage"

dotenv.config()

export default function Users() {
    const [users, setUsers] = useState([])
    const [removeLoading, setRemoveLoading] = useState(false)
    const { clientePermissao } = useContext(ClienteContext)
    const router = useRouter()
    const session = getDataFromLocalStorage() || null
    // const session = JSON.parse(localStorage.getItem("user_logged"))
    const baseUrl = process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT ? process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT : process.env.NEXT_PUBLIC_BASE_URL_PRODUCTION;

    useEffect(() => {
        if (clientePermissao === null || clientePermissao === undefined || !clientePermissao) {
            router.push('/')
        }
        async function getUsers() {
            try {
                const response = await fetch(`${baseUrl}/users/admin`, {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json",
                        "Authorization": "Bearer" + " " + session.token
                    }
                })

                if (response.status === 200) {
                    const dados = await response.json()
                    setUsers(dados)
                    setRemoveLoading(true)
                } else {
                    toast.error("Sem autenticação.")
                    return
                }
            } catch (error) {
                toast.error("Erro.")
                return
            }
        }
        getUsers()
    }, [])

    async function deleteUser(id) {
        const response = await fetch(`${baseUrl}/users/delete/` + id, {
            method: "DELETE",
            headers: {
                "Content-type": "application/json",
                "Authorization": "Bearer" + " " + session.token
            }
        })
        if (response.status === 201) {
            const dados = await response.json()
            setUsers(dados)
            setRemoveLoading(true)
        } else {
            toast.error("Sem autorização.")
            return
        }
        const novosDados = users.filter(user => user.id != id)
        setUsers(novosDados)
    }

    async function filtraDados(data) {
        if (data.pesq.length < 2) {
            Swal.fire("Digite, no mínimo, 2 caracteres")
            return
        }

        const pesquisa = data.pesq.toUpperCase()

        const response = await fetch(`${baseUrl}/users`, {
            method: 'GET',
            headers: {
                "Content-type": "application/json",
                "Authorization": "Bearer" + " " + session.token
            }
        })
        const dados = await response.json()

        const novosDados = dados.filter(user =>
            user.name.toUpperCase().includes(pesquisa) || user.username.toUpperCase().includes(pesquisa) || user.permission.toUpperCase().includes(pesquisa)
        )

        if (novosDados.length == 0) {
            Swal.fire("Não há usuários com a palavra chave informada...")
            return
        }

        setUsers(novosDados)

        // busca os dados da API já com o filtro
        // --------------------------------------
        // const response = await fetch("http://localhost:3004/filmes?titulo="+data.pesq)
        // const dados = await response.json()
        // setFilmes(dados)
    }

    async function mostraTodos() {
        const response = await fetch(`${baseUrl}/users`, {
            method: 'GET',
            headers: {
                "Content-type": "application/json",
                "Authorization": "Bearer" + " " + session.token
            }
        })
        const dados = await response.json()
        setUsers(dados)
    }

    const listUsers = users.map(user => (
        <UsersListTable key={user.id}
            user={user}
            exclui={() => deleteUser(user.id)}
            altera={() => router.push('update_user/' + user.id)}
            consulta={() => router.push('user/' + user.id)}
        />
    ))


    return (
        <div className='content'>
            {clientePermissao === "Master" &&
                <div className="container">
                    <div className="row mt-3">
                        <div className="col-12 text-center col-sm-12 col-md-2">
                            <h2 className="mt-2">Usuários</h2>
                        </div>
                        <div className="col-12 col-sm-8 col-md-8 d-flex align-items-center justify-content-center">
                            <Pesquisa filtra={filtraDados} mostra={mostraTodos} />
                        </div>
                        <div className="col-12 my-3 col-sm-4 col-md-2 d-flex align-items-center justify-content-center">
                            <button className="btn btn-success"
                                onClick={() => router.push("/create_user")}>
                                Cadastrar
                            </button>
                        </div>
                    </div>
                    <div className="overflow-scroll visible-scrollbar" style={{ maxHeight: 500 }}>
                        <table className="table table-striped overflow-auto">
                            <thead>
                                <tr>
                                    <th className="text-center" style={{maxWidth: 15}}>Ações</th>
                                    <th>Nome</th>
                                    <th>Usuário</th>
                                    <th>Permissão</th>
                                </tr>
                            </thead>
                            <tbody>
                                {listUsers}
                            </tbody>
                        </table>
                    </div>
                    {!removeLoading && <Loading />}
                    {listUsers.length === 0 && removeLoading ? <NoPublications /> : null}
                </div>
            }
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
            />
        </div>
    )
}