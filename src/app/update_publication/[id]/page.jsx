'use client'
import { ClienteContext } from "@/contexts/cliente";
import { useParams, useRouter } from "next/navigation";
import { useContext, useEffect, useRef, useState } from "react";
import { useForm } from "react-hook-form";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import { Editor } from '@tinymce/tinymce-react';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import getDataFromLocalStorage from "@/utils/getDataFromLocalStorage";

const animatedComponents = makeAnimated();

export default function UpdatePublication() {
    const editorRef = useRef(null);
    const params = useParams()
    const { register, handleSubmit, reset } = useForm()
    const { clientePermissao } = useContext(ClienteContext)
    const router = useRouter()
    const [ descricao, setDescricao ] = useState("")
    const [categoriesOptions, setCategoriesOptions] = useState([])
    const [selectedCategory, setSelectedCategory] = useState(null)
    const [visibility, setVisibility] = useState(null)
    const baseUrl = process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT ? process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT : process.env.NEXT_PUBLIC_BASE_URL_PRODUCTION;
    const session = getDataFromLocalStorage() || null
    // const session = JSON.parse(localStorage.getItem("user_logged"))

    useEffect(() => {
        if(clientePermissao === null || clientePermissao === undefined || !clientePermissao) {
            router.push('/')
        }
        reset()
        try {
            async function getPublication() {
                const response = await fetch(`${baseUrl}/publications/search_id/` + params.id, {
                    method: 'GET',
                    headers: {
                      "Content-type": "application/json",
                      "Authorization": "Bearer" + " " + session.token
                    }
                })
                const categories = await fetch(`${baseUrl}/categories`, {
                    method: "GET",
                    headers: {
                        "Content-type": "application/json",
                        "Authorization": "Bearer" + " " + session.token
                    }
                })
                const data = await response.json()
                
                const categoriesResponse = await categories.json()

                const categoriesOptions = categoriesResponse.map(category => ({
                    'value': category.id,
                    'label': category.name
                }))

                if(selectedCategory == {} || selectedCategory == null || selectedCategory == undefined) {
                    const initialCategoryConfig = {
                        'label': data.category.name,
                        'value': data.category.id
                    }
                    
                    setSelectedCategory(initialCategoryConfig)
                }
                
                setDescricao(data.description)
                setCategoriesOptions(categoriesOptions)
                setVisibility(data.isPublic)
                
                if(response.status === 200) {
                    reset({
                        title: data.title,
                        subtitle: data.subtitle,
                        category: data.category,
                        user_id: data.user_id,
                    })
                } else {
                    toast.error("Sem autenticação.")
                    return
                }
            }
            getPublication()
        } catch (error) {
            toast.error("Erro.")
            return
        }
    }, [])

    async function update(data) { 
        const description = editorRef.current.getContent()
        let category_id = selectedCategory.value
        let isPublic = visibility == true ? "1" : "0"

        const publication = await fetch(`${baseUrl}/publications/update/` + params.id,
            {
                method: "PUT",
                headers: { 
                    "Content-type": "application/json",
                    "Authorization": "Bearer" + " " + session.token
                },
                body: JSON.stringify({ ...data, description, isPublic, category_id })
            },
        )
        if (publication.status == 201) {
            toast.success("Publicação alterada com sucesso!")
        } else {
            toast.error("Erro... Não foi possível concluir a alteração.")
        }
    }
    

    return (
        <div className='content'>
            {(clientePermissao === "Editor" || clientePermissao === "Master") &&
            <div className="container">
                <h2 className="mt-2">Alterar Publicação</h2>
                <form onSubmit={handleSubmit(update)}>
                    <div className="row">
                        <div className="col-sm-6">
                        <label htmlFor="title" className="form-label">Título da Publicação:</label>
                        <input type="text" className="form-control" id="title" {...register("title")} required />
                        </div>
                        <div className="col-sm-6">
                        <label htmlFor="subtitle" className="form-label">Subtítulo da Publicação:</label>
                        <input type="text" step="0.10" className="form-control" id="subtitle" {...register("subtitle")} required />
                        </div>
                    </div>
        
                    <div className="row my-4">
                        <div className="col-sm-6">
                            <label htmlFor="category" className="form-label">Categoria:</label>
                            <Select
                                closeMenuOnSelect={true}
                                components={animatedComponents}
                                isSearchable
                                options={categoriesOptions}
                                onChange={(item) => setSelectedCategory(item)}
                                className="z-3"
                                value={selectedCategory}
                                placeholder='Selecione'
                            />
                        </div>
                    </div>
        
                    <Editor
                        apiKey='ripemour1w42a5x27zbm6zhhi15g0o8o4smfk2xqe0422oqo'
                        onInit={(_evt, editor) => editorRef.current = editor}
                        initialValue={descricao}
                        init={{
                            height: 300,
                            menubar: false,
                            plugins: [
                            'advlist', 'autolink', 'lists', 'link', 'image', 'charmap', 'preview',
                            'anchor', 'searchreplace', 'visualblocks', 'code', 'fullscreen',
                            'insertdatetime', 'media', 'table', 'code', 'help', 'wordcount'
                            ],
                            toolbar: 'undo redo | blocks | ' +
                            'bold italic forecolor | alignleft aligncenter ' +
                            'alignright alignjustify | bullist numlist outdent indent | ' +
                            'removeformat | help',
                            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
                        }}
                    />

                    <div className="row mt-4">
                        <div className="col-sm-12">
                            <p>Visibilidade:</p>
                            <div className="form-check">
                                <input className="form-check-input" type="radio" name="exampleRadios"
                                    id="exampleRadios1" value="0" onChange={e => setVisibility(e.target.value)} checked={visibility === false || visibility === '0'} required />
                                <label className="form-check-label" for="exampleRadios1">
                                    Interno
                                </label>
                            </div>
                            <div className="form-check">
                                <input className="form-check-input" type="radio" name="exampleRadios"
                                    id="exampleRadios2" value="1" onChange={e => setVisibility(e.target.value)} checked={visibility === true || visibility === '1'} required />
                                <label className="form-check-label" for="exampleRadios2">
                                    Público
                                </label>
                            </div>
                        </div>
                    </div>
        
                    <input type="submit" value="Alterar" className="btn btn-success me-3 my-3" />
                    <input type="button" value="Voltar" className="btn btn-warning my-3"
                        onClick={() => router.push('/publications')} />
    
                </form>
                <ToastContainer
                    position="top-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    theme="colored"
                />
            </div>
            }
        </div>
    )
}