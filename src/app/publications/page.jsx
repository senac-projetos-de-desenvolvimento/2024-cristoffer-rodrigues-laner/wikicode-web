'use client'
import { useContext, useEffect, useState } from "react"
import { useRouter } from "next/navigation"
import PublicationsListTable from "@/components/PublicationsListTable"
import NoPublications from "@/components/NoPublications"
import Loading from "@/components/Loading"
import Pesquisa from "@/components/Pesquisa"
import Swal from "sweetalert2"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import { ClienteContext } from "@/contexts/cliente"
import "./style.css"
import dotenv from "dotenv"
import getDataFromLocalStorage from "@/utils/getDataFromLocalStorage"

dotenv.config()

export default function Publications() {
  const [publications, setPublications] = useState([])
  const [removeLoading, setRemoveLoading] = useState(false)
  const { clientePermissao } = useContext(ClienteContext)
  const router = useRouter()
  const session = getDataFromLocalStorage() || null
  // const session = JSON.parse(localStorage.getItem("user_logged"))
  const baseUrl = process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT ? process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT : process.env.NEXT_PUBLIC_BASE_URL_PRODUCTION;


  useEffect(() => {
    if(clientePermissao === null || clientePermissao === undefined || !clientePermissao) {
      router.push('/')
    }
    async function getPublication() {
      try {
        const response = await fetch(`${baseUrl}/publications/admin`, {
          method: 'GET',
          headers: {
            "Content-type": "application/json",
            "Authorization": "Bearer" + " " + session.token
          }
        })
        
        if(response.status === 200) {
          const dados = await response.json()
          setPublications(dados)
          setRemoveLoading(true)
        } else {
          toast.error("Sem autenticação.")
          return
        }
      } catch (error) {
        toast.error("Erro.")
        return
      }
    }
    getPublication()
  }, [])

  async function deletePublication(id) {
    const response = await fetch(`${baseUrl}/publications/delete/` + id, {
      method: "DELETE",
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer" + " " + session.token
      }
    })
    if(response.status === 201) {
      const dados = await response.json()
      setPublications(dados)
      setRemoveLoading(true)
    } else {
      toast.error("Sem autorização.")
      return
    }
    const novosDados = publications.filter(publi => publi.id != id)
    setPublications(novosDados)
  }

  async function filtraDados(data) {
    if (data.pesq.length < 2) {
      Swal.fire("Digite, no mínimo, 2 caracteres")
      return
    }

    const pesquisa = data.pesq.toUpperCase()
    
    const response = await fetch(`${baseUrl}/publications`, {
      method: 'GET',
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer" + " " + session.token
      }
    })
    const dados = await response.json()

    const novosDados = dados.filter(publi =>
      publi.title.toUpperCase().includes(pesquisa) ||
      publi.user.name.toUpperCase().includes(pesquisa) ||
      publi.category.name.toUpperCase().includes(pesquisa) ||
      publi.createdAt.toUpperCase().includes(pesquisa)
    )

    if (novosDados.length == 0) {
      Swal.fire("Não há publicações com a palavra chave informada...")
      return
    }

    setPublications(novosDados)
  }

  async function mostraTodos() {
    const response = await fetch(`${baseUrl}/publications`, {
      method: 'GET',
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer" + " " + session.token
      }
    })
    const dados = await response.json()
    setPublications(dados)
  }

  const listPublications = publications.map(publi => (
    <PublicationsListTable key={publi.id}
      publication={publi}
      exclui={() => deletePublication(publi.id)}
      altera={() => router.push('update_publication/' + publi.id)}
      consulta={() => router.push('publication/' + publi.id)}
    />
  ))


  return (
    <div className='content'>
      {!(clientePermissao === "Leitor") &&
          <div className="container">
            <div className="row mt-3">
              <div className="col-12 text-center col-sm-12 col-md-2">
                <h2 className="mt-2">Publicações</h2>
              </div>
              <div className="col-12 col-sm-8 col-md-8 d-flex align-items-center justify-content-center">
                <Pesquisa filtra={filtraDados} mostra={mostraTodos} />
              </div>
              <div className="col-12 my-3 col-sm-4 col-md-2 d-flex align-items-center justify-content-center">
                <button className="btn btn-success" 
                  onClick={() => router.push("/create_publication")}>
                    Cadastrar
                </button>
              </div>
            </div>
            <div className="overflow-scroll visible-scrollbar" style={{maxHeight: 500}}>
                <table className="table table-striped overflow-auto">
                    <thead>
                        <tr className='border-bottom'>
                            <th className="text-start border-0" style={{maxWidth: 15}}>Ações</th>
                            <th className='border-0'>Título</th>
                            <th className='border-0'>Categoria</th>
                            <th className='border-0'>Autor</th>
                            <th className='border-0'>Data</th>
                            <th className='border-0'>Visibilidade</th>
                        </tr>
                    </thead>
                    <tbody>
                        {listPublications}
                    </tbody>
                </table>
            </div>
            {!removeLoading && <Loading />}
            {listPublications.length === 0 && removeLoading ? <NoPublications /> : null}
        </div>
        
      }
      <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="colored"
          />
    </div>
  )
}