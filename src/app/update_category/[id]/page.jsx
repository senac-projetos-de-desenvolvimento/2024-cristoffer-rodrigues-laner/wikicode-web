'use client'
import { ClienteContext } from "@/contexts/cliente";
import { useParams, useRouter } from "next/navigation";
import { useContext, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import dotenv from "dotenv"
import getDataFromLocalStorage from "@/utils/getDataFromLocalStorage";

dotenv.config()

export default function CreateUser() {
  const { register, handleSubmit, reset } = useForm({});
  const { clientePermissao } = useContext(ClienteContext)
  const [category, setCategory] = useState("")
  const router = useRouter()
  const session = getDataFromLocalStorage() || null
  // const session = JSON.parse(localStorage.getItem("user_logged"))
  const params = useParams()
  const baseUrl = process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT ? process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT : process.env.NEXT_PUBLIC_BASE_URL_PRODUCTION;


  useEffect(() => {
        if(clientePermissao === null || clientePermissao === undefined || !clientePermissao) {
        router.push('/')
        }
        try {
            async function getCategory() {
                const category = await fetch(`${baseUrl}/categories/search_id/` + params.id, {
                    method: "GET",
                    headers: {
                        "Content-type": "application/json",
                        "Authorization": "Bearer" + " " + session.token
                    }
                })
                const data = await category.json()

                if(category.status === 200) {
                    reset({
                        name: data.name,
                    })
                }
            }
            getCategory()
            
        } catch (error) {
            toast.error("Erro.")
            return
        }
  }, [])

  async function update(data) {
    const request = await fetch(`${baseUrl}/categories/update/` + params.id,
      {
        method: "PUT",
        headers: {
          "Content-type": "application/json",
          "Authorization": "Bearer" + " " + session.token
        },
        body: JSON.stringify({ ...data })
      },
    )
    const response = await request.json()

    if (request.status == 201) {
      toast.success("Categoria alterada com sucesso!")
    } else {
      toast.error(response.error)
    }
  }

  return (
    
    <div className='content'>
      {clientePermissao !== "Leitor" &&
      <div className="container">
        <h2 className="mt-2">Cadastrar Categoria</h2>
        <form onSubmit={handleSubmit(update)}>
          <div className="row">
            <div className="col-sm-4 my-3">
              <label htmlFor="name" className="form-label">Nome:</label>
              <input type="text" className="form-control" id="name" placeholder="Escreva um nome..." {...register("name")} required />
            </div>
          </div>
  
          <input type="submit" value="Alterar" className="btn btn-success me-3" />
          <input type="button" value="Voltar" className="btn btn-warning"
                        onClick={() => router.push('/categories')} />
        </form>
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="colored"
        />
      </div>
      }
    </div>
    
  )
}