'use client'
import { ClienteContext } from "@/contexts/cliente";
import { useRouter } from "next/navigation";
import { useContext, useEffect } from "react";
import { useForm } from "react-hook-form";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import dotenv from "dotenv"
import getDataFromLocalStorage from "@/utils/getDataFromLocalStorage";


dotenv.config()

export default function CreateUser() {
  const { register, handleSubmit, reset } = useForm({});
  const { clientePermissao } = useContext(ClienteContext)
  const router = useRouter()
  const session = getDataFromLocalStorage() || null
  // const session = JSON.parse(localStorage.getItem("user_logged"))
  const baseUrl = process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT ? process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT : process.env.NEXT_PUBLIC_BASE_URL_PRODUCTION;


  useEffect(() => {
    // if(clientePermissao === null || clientePermissao === undefined || !clientePermissao) {
    //   router.push('/login')
    // }
  }, [])

  async function create(data) {
    const request = await fetch(`${baseUrl}/categories/create`,
      {
        method: "POST",
        headers: {
          "Content-type": "application/json",
          "Authorization": "Bearer" + " " + session.token
        },
        body: JSON.stringify({ ...data })
      },
    )
    const response = await request.json()
    
    if (response.status == 201) {
      toast.success("Categoria cadastrada com sucesso!")
      reset()
    } else {
      toast.error(response.error)
    }
  }

  return (
    
    <div className='content'>
      {clientePermissao !== "Leitor" &&
      <div className="container">
        <h2 className="mt-2">Cadastrar Categoria</h2>
        <form onSubmit={handleSubmit(create)}>
          <div className="row">
            <div className="col-sm-4 my-3">
              <label htmlFor="name" className="form-label">Nome: <span className="text-danger fw-bold">*</span></label>
              <input type="text" className="form-control" id="name" placeholder="Escreva um nome..." {...register("name")} required />
            </div>
          </div>
  
          <input type="submit" value="Cadastrar" className="btn btn-success me-3" />
          <input type="button" value="Voltar" className="btn btn-warning"
                        onClick={() => router.push('/categories')} />
        </form>
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="colored"
        />
      </div>
      }
    </div>
    
  )
}