'use client'
import { ClienteContext } from "@/contexts/cliente";
import { useRouter } from "next/navigation";
import { useContext, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import dotenv from "dotenv"
import getDataFromLocalStorage from "@/utils/getDataFromLocalStorage";

dotenv.config()

export default function CreateUser() {
  const { register, handleSubmit, reset } = useForm({});
  const { clientePermissao } = useContext(ClienteContext)
  const router = useRouter()
  const session = getDataFromLocalStorage() || null
  // const session = JSON.parse(localStorage.getItem("user_logged"))
  const [isShow, setIsShow] = useState(false)
  const baseUrl = process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT ? process.env.NEXT_PUBLIC_BASE_URL_DEVELOPMENT : process.env.NEXT_PUBLIC_BASE_URL_PRODUCTION;

  const handlePassword = () => setIsShow(!isShow)

  useEffect(() => {
    if(clientePermissao === null || clientePermissao === undefined || !clientePermissao) {
      router.push('/')
    }
  }, [])

  async function create(data) {
    const request = await fetch(`${baseUrl}/users/create`,
      {
        method: "POST",
        headers: {
          "Content-type": "application/json",
          "Authorization": "Bearer" + " " + session.token
        },
        body: JSON.stringify({ ...data })
      },
    )
    const response = await request.json()
    
    if (response.message == "User created successfully!") {
      toast.success("Usuário cadastrado com sucesso!")
      reset()
    } else {
      toast.error(response.error)
    }
  }

  return (
    
    <div className='content'>
      {clientePermissao === "Master" &&
      <div className="container">
        <h2 className="mt-2">Cadastrar Usuário</h2>
        <form onSubmit={handleSubmit(create)}>
          <div className="row">
            <div className="col-sm-6">
              <label htmlFor="name" className="form-label">Nome: <span className="text-danger fw-bold">*</span></label>
              <input type="text" className="form-control" id="name" placeholder="Escreva um nome..." {...register("name")} required />
            </div>
            <div className="col-sm-6">
              <label htmlFor="username" className="form-label">Usuário: <span className="text-danger fw-bold">*</span></label>
              <input type="text" step="0.10" className="form-control" id="username" placeholder="Digite um login..." {...register("username")} required />
            </div>
            <div className="col-sm-6 mt-3">
              <label htmlFor="password" className="form-label">Senha: <span className="text-danger fw-bold">*</span></label>
              <input type={isShow ? "text" : "password"} step="0.10" className="form-control" id="password" placeholder="********" {...register("password")} required />
            </div>
          </div>
          <div class="form-check mt-3">
            <input class="form-check-input" type='checkbox' value="" id="flexCheckDefault" onClick={handlePassword} />
            <label class="form-check-label" for="flexCheckDefault">
              Mostrar senha
            </label>
          </div>
          <div className="row my-3">
            <div className="col-sm-4">
              <label htmlFor="permission" className="form-label">Permissão: <span className="text-danger fw-bold">*</span></label>
              <select id="permission" className="form-select" {...register("permission")} required>
                <option value="" selected disabled>Selecione</option>
                <option value="Leitor">Leitor</option>
                <option value="Editor">Editor</option>
                <option value="Master">Master</option>
              </select>
            </div>
          </div>
          
          <input type="submit" value="Cadastrar" className="btn btn-success me-3" />
          <input type="button" value="Voltar" className="btn btn-warning"
                        onClick={() => router.push('/users')} />
        </form>
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="colored"
        />
      </div>
      }
    </div>
    
  )
}