import loading from '../../public/assets/fade-stagger-circles.svg'
import Image from "next/image";
import './styles/loading.css'

export default function Loading() {
    return (
        <div className="loader_container">
           <Image className='loader' src={loading} alt="Carregando as informações..." />
        </div>
    )
}