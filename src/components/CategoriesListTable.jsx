import 'bootstrap-icons/font/bootstrap-icons.css';
import Swal from 'sweetalert2'
import './styles/publicationlisttable.css'

export default function CategoriesListTable(props) {
  function confirmDelete(id, name) {
    Swal.fire({
      title: `Confirma a exclusão da categoria "${name}"?`,
      text: "Esta operação não poderá ser desfeita!",
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Excluir!',
      cancelButtonText: 'Voltar',
      allowEnterKey: false
    }).then((result) => {
      if (result.isConfirmed) {
        props.exclui(id)
      }
    })
  }

  return (
    <tr className='border-bottom'>
      <td className='border-0 text-nowrap'>
        <i className="bi bi-pencil-square text-info pe-3 update-button" style={{fontSize: 20, cursor: 'pointer'}}
           onClick={props.altera}
           title="Alterar"
        ></i>
        <i className="bi bi-trash3-fill text-danger pe-3 delete-button" style={{fontSize: 20, cursor: 'pointer'}}
            onClick={() => confirmDelete(props.category.id, props.category.name)}
            title="Excluir"
        ></i>
      </td>
      <td className="border-0 w-100" style={{maxWidth: 60, textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden'}}>{props.category.name}</td>
    </tr>    
  )
}