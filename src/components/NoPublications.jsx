import Image from "next/image";
import './styles/nopublications.css'

export default function NoPublications() {   
  return (
    <div className="container w-100 d-flex flex-column align-items-center justify-content-center">
        <div className="d-flex flex-column align-items-center justify-content-center">
            <Image src="/assets/empty-box.jpg" priority alt="Caixa vazia com mosca saindo dela." width="360" height="360" />
            <h3>Não há nada para exibir.</h3>
        </div>
    </div>
  )
}