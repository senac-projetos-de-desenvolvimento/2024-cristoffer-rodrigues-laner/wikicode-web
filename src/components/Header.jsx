'use client'

import Link from "next/link";
import Image from "next/image";
import './styles/header.css'
import '../app/globals.css'
import { useContext } from "react";
import { ClienteContext } from "@/contexts/cliente";
import Toggler from "./Toggler";
import deleteDataFromLocalStorage from "@/utils/deleteDataFromLocalStorage";

export default function Header() {
  const { token, clienteNome } = useContext(ClienteContext)
  
  const logout = () => {
    deleteDataFromLocalStorage()
    // localStorage.removeItem('user_logged')
  }

  return (
    <>
      {token &&
      <header style={{zIndex: '100'}} className="bg-white shadow border-0">
          <div className="ms-1 my-1 row d-flex justify-content-between align-items-center">
            <Link className="col" href="/home">
              <Image src="/assets/Logo wikicode.png" priority alt="Logo" width="240" height="36" className="d-none d-md-inline-block my-1" />
              <Image src="/assets/Logo wikicode.png" priority alt="Logo" width="200" height="30" className="d-inline-block d-md-none mt-1" />
            </Link>
            <Toggler />
            <div className="col me-2 avatar d-none d-lg-flex align-items-center justify-content-end gap-2 ">
              <p className="mb-0 fs-5">Olá, {clienteNome}</p>
              <Image src="/assets/avatar.png" priority alt="Avatar" width="44" height="44" className="avatar" />
              <a href="/" onClick={() => logout()}>
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#3E6E9B" className="bi bi-box-arrow-right" viewBox="0 0 16 16">
                    <path fillRule="evenodd" d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0z"/>
                    <path fillRule="evenodd" d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708z"/>
                  </svg>
              </a>
            </div>
          </div>
      </header>
      }
    </>
    
  )
}