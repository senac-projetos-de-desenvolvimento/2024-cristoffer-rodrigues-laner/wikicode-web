import Image from "next/image";
import './styles/comments.css'
import { adjustDateHourToLOCFormat } from "@/utils/adjustDateHour";
import { useContext } from "react";
import { ClienteContext } from "@/contexts/cliente";
import Swal from "sweetalert2";

export default function Comments(props) {
    const { clientePermissao } = useContext(ClienteContext)
    const commentUserName = props.comment.user ? props.comment.user.name : null;
    const commentClientName = props.comment.client ? props.comment.client.name : null;

    function confirmDelete(id, name) {
        Swal.fire({
            title: `Confirma a exclusão do comentário de "${name}"?`,
            text: "Esta operação não poderá ser desfeita!",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Excluir!',
            cancelButtonText: 'Voltar',
            allowEnterKey: false
        }).then((result) => {
            if (result.isConfirmed) {
                props.exclui(id)
            }
        })
    }

    const dateAdjusted = adjustDateHourToLOCFormat(props.comment.date)

    return (
        <div className="container py-3">
            <div className="d-flex gap-3">
            <div className="d-flex flex-column justify-content-center align-items-center">
                    {
                            (
                                commentUserName !== null &&
                                <Image src="/assets/avatar.png" alt="Avatar" width="60" height="60" className="avatar" />
                            ) || (
                                commentClientName !== null &&
                                <Image src="/assets/avatar-client.png" alt="Avatar" width="60" height="60" className="avatar" />
                            )
                        }
                    {((clientePermissao === "Master") || (clientePermissao === "Editor")) &&
                        <i className="bi bi-trash3-fill text-danger delete" style={{ fontSize: 20, cursor: 'pointer' }}
                            onClick={() => confirmDelete(props.comment.id, commentUserName !== null ? commentUserName : commentClientName)}
                            title="Excluir comentário"
                        ></i>
                    }
                </div>
                <div className="comments w-100">
                    <div className="d-flex flex-column flex-lg-row align-items-lg-end justify-center gap-2 mb-3">
                        {
                            (
                                    commentUserName !== null &&
                                <>
                                    <p className="name text-uppercase fw-bold">{commentUserName} <span className="badge rounded-pill bg-secondary">ADMIN</span></p>
                                </>
                            ) || (
                                commentClientName !== null &&
                                <>
                                    <p className="clientname text-uppercase fw-bold">{commentClientName}</p>
                                </>
                            )
                        }
                        <span className="comment-date text-uppercase fw-medium">{dateAdjusted}</span>
                    </div>
                    <p className="comment">{props.comment.comment}</p>
                </div>
            </div>
        </div>
    )
}