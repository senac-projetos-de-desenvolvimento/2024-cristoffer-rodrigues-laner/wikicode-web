'use client'

import Link from "next/link";
import './styles/navbar.css'
import { useContext } from "react";
import { ClienteContext } from "@/contexts/cliente";
import { useSelectedLayoutSegment } from 'next/navigation';

export default function Navbar() {
  const { token } = useContext(ClienteContext)
  const { clientePermissao } = useContext(ClienteContext)
  const segment = useSelectedLayoutSegment()

  return (
    <div className="d-none d-lg-block">
      {token &&
        <nav>
          <div className="container-fluid p-0">
            <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
              <li className={`nav-item py-2 ps-3 translate ${segment == 'home' ? "active" : ""}`}>
                <Link href="/home" className="nav-link text-white d-flex align-items-center gap-3">
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="white" className="bi bi-house-door-fill" viewBox="0 0 16 16">
                    <path className="text-center" d="M6.5 14.5v-3.505c0-.245.25-.495.5-.495h2c.25 0 .5.25.5.5v3.5a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5"/>
                  </svg>
                  <p className="mb-0"><small>INÍCIO</small></p>
                </Link>
              </li>
              {/* <li className={`nav-item py-2 ps-3 translate ${segment == 'versions' ? "active" : ""}`}>
                <Link href="/versions" className="nav-link text-white d-flex align-items-center gap-3">
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="white" className="bi bi-newspaper" viewBox="0 0 16 16">
                    <path className="text-center" d="M0 2.5A1.5 1.5 0 0 1 1.5 1h11A1.5 1.5 0 0 1 14 2.5v10.528c0 .3-.05.654-.238.972h.738a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 1 1 0v9a1.5 1.5 0 0 1-1.5 1.5H1.497A1.497 1.497 0 0 1 0 13.5zM12 14c.37 0 .654-.211.853-.441.092-.106.147-.279.147-.531V2.5a.5.5 0 0 0-.5-.5h-11a.5.5 0 0 0-.5.5v11c0 .278.223.5.497.5z"/>
                    <path className="text-center" d="M2 3h10v2H2zm0 3h4v3H2zm0 4h4v1H2zm0 2h4v1H2zm5-6h2v1H7zm3 0h2v1h-2zM7 8h2v1H7zm3 0h2v1h-2zm-3 2h2v1H7zm3 0h2v1h-2zm-3 2h2v1H7zm3 0h2v1h-2z"/>
                  </svg>
                  <p className="mb-0"><small>VERSÕES (X)</small></p>
                </Link>
              </li>
              <li className={`nav-item py-2 ps-3 translate ${segment == 'guides' ? "active" : ""}`}>
                <Link href="/guides" className="nav-link text-white d-flex align-items-center gap-3">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" className="bi bi-book-fill" viewBox="0 0 16 16">
                    <path d="M8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783"/>
                  </svg>
                  <p className="mb-0 fs-7"><small>GUIAS E MANUAIS (X)</small></p>
                </Link>
              </li> */}
              {(clientePermissao == "Editor" || clientePermissao == "Master") &&
                <li className={`nav-item py-2 ps-3 translate ${segment == 'categories' ? "active" : ""}`}>
                  <Link href="/categories" className="nav-link text-white d-flex align-items-center gap-3">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" className="bi bi-tags-fill" viewBox="0 0 16 16">
                      <path d="M2 2a1 1 0 0 1 1-1h4.586a1 1 0 0 1 .707.293l7 7a1 1 0 0 1 0 1.414l-4.586 4.586a1 1 0 0 1-1.414 0l-7-7A1 1 0 0 1 2 6.586zm3.5 4a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3"/>
                      <path d="M1.293 7.793A1 1 0 0 1 1 7.086V2a1 1 0 0 0-1 1v4.586a1 1 0 0 0 .293.707l7 7a1 1 0 0 0 1.414 0l.043-.043z"/>
                    </svg>
                    <p className="mb-0 fs-6"><small>CATEGORIAS</small></p>
                  </Link>
                </li>
              }
              {(clientePermissao == "Editor" || clientePermissao == "Master") &&
                <li className={`nav-item py-2 ps-3 translate ${segment == 'publications' ? "active" : ""}`}>
                  <Link href="/publications" className="nav-link text-white d-flex align-items-center gap-3">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" className="bi bi-pencil-square" viewBox="0 0 16 16">
                      <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                      <path fillRule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"/>
                    </svg>
                    <p className="mb-0 fs-6"><small>PUBLICAÇÕES</small></p>
                  </Link>
                </li>
              }
              {(clientePermissao == "Master") &&
                <li className={`nav-item py-2 ps-3 translate ${segment == 'users' ? "active" : ""}`}>
                  <Link href="/users" className="nav-link text-white d-flex align-items-center gap-3">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" className="bi bi-people-fill" viewBox="0 0 16 16">
                      <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6m-5.784 6A2.24 2.24 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.3 6.3 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1zM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5"/>
                    </svg>
                    <p className="mb-0 fs-6"><small>USUÁRIOS</small></p>
                  </Link>
                </li>
              }
              
            </ul>
          </div>
        </nav>
      }
    </div>
  )
}