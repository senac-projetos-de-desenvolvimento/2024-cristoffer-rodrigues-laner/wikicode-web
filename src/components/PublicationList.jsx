import { useEffect, useState } from 'react'
import '../app/globals.css'
import './styles/publicationlist.css'
import {adjustDateHourToLOCFormat} from '@/utils/adjustDateHour'
import Link from 'next/link'

export default function PublicationList(props) {
    const [participants, setParticipants] = useState([])

    useEffect(() => {
        let participantesArr = []

        props.publication.Participants.map((participant) => {
            participantesArr.push(participant.name)
        })

        setParticipants(participantesArr)
    }, [])

  const dateAdjusted = adjustDateHourToLOCFormat(props.publication.createdAt)

  return (
    <li className="list-group py-3 px-3 text-dark list-color-bg">
        <div>
            <Link className="nav-link" href={"/publication/" + props.publication.id} target='_blank'>
                <h4 className="title" style={{overflow: 'hidden', textOverflow: '...continuar lendo'}}>{props.publication.title}</h4>
                <h5 className="subtitle" style={{overflow: 'hidden', textOverflow: '...continuar lendo'}}>{props.publication.subtitle}</h5>
                <div className="d-flex flex-wrap gap-1 gap-lg-3 flex-lg-row">
                    <div className="date d-flex align-center justify-center gap-2 m-0">
                        <span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-calendar3" viewBox="0 0 16 16">
                                <path d="M14 0H2a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2M1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857z"/>
                                <path d="M6.5 7a1 1 0 1 0 0-2 1 1 0 0 0 0 2m3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2m3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2m-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2m3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2m3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2m3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2m-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2m3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2m3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2"/>
                            </svg>
                        </span>
                        <p>Publicado em {dateAdjusted}</p>
                    </div>
                    <div className="author d-flex align-center justify-center gap-2 m-0">
                        <span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-person-fill" viewBox="0 0 16 16">
                                <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6"/>
                            </svg>
                        </span>
                        <p>por {props.publication.user.name}</p>
                    </div>
                    {
                        participants.length > 0 ?
                            <div className="participants d-flex gap-2">
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-people-fill" viewBox="0 0 16 16">
                                        <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6m-5.784 6A2.24 2.24 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.3 6.3 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1zM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5"/>
                                    </svg>
                                </span>
                                <p>participantes: {participants.join(", ")}</p>
                            </div>
                        : 
                            ""
                    }
                </div>
            </Link>
        <hr className='hr mb-0' />
      </div>
    </li>
  )
}