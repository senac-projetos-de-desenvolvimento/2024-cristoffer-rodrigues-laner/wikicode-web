import { adjustDateToLOCFormat } from '@/utils/adjustDate';
import 'bootstrap-icons/font/bootstrap-icons.css';
import Swal from 'sweetalert2'
import './styles/publicationlisttable.css'
import Link from 'next/link';
import { useContext } from 'react';
import { ClienteContext } from '@/contexts/cliente';

export default function PublicationsListTable(props) {
    const { clientePermissao } = useContext(ClienteContext)

    function confirmDelete(id, title) {
        Swal.fire({
            title: `Confirma a exclusão da publicação "${title}"?`,
            text: "Esta operação não poderá ser desfeita!",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Excluir!',
            cancelButtonText: 'Voltar',
            allowEnterKey: false
        }).then((result) => {
            if (result.isConfirmed) {
                props.exclui(id)
            }
        })
    }
    

    const adjustedDate = adjustDateToLOCFormat(props.publication.createdAt)

    return (
        <tr className='border-bottom'>
            <td className='text-start border-0' style={{ width: 15, whiteSpace: 'nowrap', overflow: 'ellipsis' }}>
                <i className="bi bi-pencil-square text-info me-3 update-button" style={{ fontSize: 20, cursor: 'pointer' }}
                    onClick={props.altera}
                    title="Alterar"
                ></i>
                {clientePermissao === "Master" &&
                    <i className="bi bi-trash3-fill text-danger me-3 delete-button" style={{ fontSize: 20, cursor: 'pointer' }}
                        onClick={() => confirmDelete(props.publication.id, props.publication.title)}
                        title="Excluir"
                    ></i>
                }
            </td>
            <td className="border-0" style={{ minWidth: 100, whiteSpace: 'nowrap', overflow: 'hidden' }}>
                {
                    <div className='d-flex gap-1 align-center justify-center'>
                        <Link href={"/publication/" + props.publication.id} target='_blank'>
                            {props.publication.title}
                            <span className='ms-2'>
                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" className="bi bi-box-arrow-up-right" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5"/>
                                    <path fill-rule="evenodd" d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0z"/>
                                </svg>
                            </span>
                        </Link>
                    </div>
                }
            </td>
            
            <td className="border-0" style={{ minWidth: 60, whiteSpace: 'nowrap', overflow: 'hidden' }}>{props.publication.category === null ? "Sem categoria ": props.publication.category.name}</td>
            <td className="border-0" style={{ minWidth: 60, whiteSpace: 'nowrap', overflow: 'hidden' }}>{props.publication.user.name}</td>
            <td className='border-0' style={{ height: '49px' }}>{adjustedDate}</td>
            <td className='border-0' style={{ minWidth: 150, height: '49px' }}>
                {
                    props.publication.isPublic == true ?
                        <div className='d-flex gap-2 align-center justify-center'>
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="#3E6E9B" className="bi bi-circle-fill" viewBox="0 0 16 16">
                                    <circle cx="8" cy="8" r="8" />
                                </svg>
                            </span>
                            <p className='mb-0'>Público</p>
                        </div>
                        :
                        <div className='d-flex gap-2 align-center justify-center'>
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="#FFA500" className="bi bi-circle-fill" viewBox="0 0 16 16">
                                    <circle cx="8" cy="8" r="8" />
                                </svg>
                            </span>
                            <p className='mb-0'>Interno</p>
                        </div>
                }
            </td>
        </tr>
    )
}