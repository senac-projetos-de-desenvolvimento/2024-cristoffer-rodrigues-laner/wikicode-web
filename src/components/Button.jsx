import Spinner from './Spinner'
import './styles/loading.css'

export default function Button(props) {
    return (
        <button className={`btn btn-${props.color} w-100 py-2 mt-3 ${props.loadingButton ? 'disabled' : ""}`} onClick={props.onClick} type='submit'>
            {props.loadingButton && <Spinner />}
            {!props.loadingButton && props.title}
        </button>
    )
}