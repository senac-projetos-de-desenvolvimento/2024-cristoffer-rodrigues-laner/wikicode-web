import 'bootstrap-icons/font/bootstrap-icons.css';
import Swal from 'sweetalert2'
// import '../app/globals.css'
import './styles/publicationlisttable.css'
import { useContext } from 'react';
import { ClienteContext } from '@/contexts/cliente';

export default function UsersListTable(props) {
  const { clienteId } = useContext(ClienteContext)

  function confirmDelete(id, name) {
    Swal.fire({
      title: `Confirma a exclusão do usuário "${name}"?`,
      text: "Esta operação não poderá ser desfeita!",
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Excluir!',
      cancelButtonText: 'Voltar',
      allowEnterKey: false
    }).then((result) => {
      if (result.isConfirmed) {
        props.exclui(id)
        Swal.fire(
          'Excluído!',
          'Usuário excluído com sucesso',
          'success'
        )
      }
    })
  }


  return (
    <tr>
      <td className='text-center' style={{ width: 15, whiteSpace: 'nowrap', overflow: 'ellipsis' }}>
        <i className="bi bi-pencil-square text-info pe-3 update-button" style={{fontSize: 20, cursor: 'pointer'}}
           onClick={props.altera}
           title="Alterar"
        ></i>
        {clienteId !== props.user.id &&
          <i className="bi bi-trash3-fill text-danger pe-3 delete-button" style={{fontSize: 20, cursor: 'pointer'}}
             onClick={() => confirmDelete(props.user.id, props.user.name)}
             title="Excluir"
          ></i>
        }
      </td>
      <td style={{minWidth: 100, whiteSpace: 'nowrap'}}>{props.user.name}</td>
      <td style={{minWidth: 100, whiteSpace: 'nowrap'}}>{props.user.username}</td>
      <td style={{minWidth: 100, whiteSpace: 'nowrap'}}>{props.user.permission}</td>
    </tr>    
  )
}